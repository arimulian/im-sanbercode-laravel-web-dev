<?php
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>";
echo "legs : " . $sheep->legs . "<br>";
echo "cold_blooded :" . $sheep->cold_blooded . "<br>" . "<br>";


$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br>";
echo "legs : " . $kodok->legs . "<br>";
echo "cold_blooded :" . $kodok->cold_blooded . "<br>";
$kodok->jump() . "<br>";

$sunggokong = new Ape("Kera sakti");
echo "Name : " . $sunggokong->name . "<br>";
echo "legs : " . $sunggokong->legs . "<br>";
echo "cold_blooded :" . $sunggokong->cold_blooded . "<br>";
$sunggokong->yell();
